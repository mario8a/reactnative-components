import React, { useEffect, useReducer } from 'react';
import { createContext } from 'react';
import { Appearance, AppState, useColorScheme } from 'react-native';
import { ThemeState, themeReducer, lightTeme, darkTheme } from './ThemeReducer';


interface ThemeContextProps {
  theme: ThemeState;
  setDarkTheme: () => void;
  setLightTheme: () => void;
}


export const ThemeContext = createContext({} as ThemeContextProps);

export const ThemeProvider = ({children}: any) => {

  // const colorScheme = useColorScheme();

  // const [theme, dispatch] = useReducer(themeReducer,  colorScheme === 'dark' ? darkTheme : lightTeme);
  const [theme, dispatch] = useReducer(
    themeReducer,
    (Appearance.getColorScheme() === 'dark') ? darkTheme : lightTeme
  );

  useEffect(() => {
    AppState.addEventListener('change', (status) => {

      if (status === 'active') {
        (Appearance.getColorScheme() === 'light')
          ? setLightTheme()
          : setDarkTheme();
      }
    });
  }, []);


  // solo funciona en IOS
  // useEffect(() => {
  //   (colorScheme === 'light')
  //     ? setLightTheme()
  //     : setDarkTheme();
  // }, [colorScheme]);

  const setDarkTheme = () => {
    dispatch({type: 'set_dark_theme'});
  };

  const setLightTheme = () => {
    dispatch({type: 'set_light_theme'});
  };

  return (
    <ThemeContext.Provider value={{
      theme,
      setDarkTheme,
      setLightTheme,
    }}>
      {children}
    </ThemeContext.Provider>
  );
};
