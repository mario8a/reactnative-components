import { useNavigation } from '@react-navigation/native';
import React, { useContext } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import { ThemeContext } from '../context/themeContext/ThemeContext';
import { MenuItem } from '../interfaces/appInterfaces';



interface Props {
  menuItem: MenuItem
}

export const FlatListMenuItem = ({menuItem}: Props) => {

  const navigation =  useNavigation();
  // const { colors } = useTheme();
  const {theme} = useContext(ThemeContext);

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={() => navigation.navigate(menuItem.component as any)}
    >
      <View style={styles.container}>
        <Icon
          name={menuItem.icon}
          color={theme.colors.primary}
          size={25}
        />
        <Text style={{...styles.itemText, color: theme.colors.text}}>{menuItem.name}</Text>

        <View style={{flex: 1}}/>
        <Icon
          name='chevron-forward-outline'
          color={theme.colors.primary}
          size={25}
          />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({

  container: {
    flexDirection: 'row',
  },
  itemText: {
    marginLeft: 10,
    fontSize: 19
  },

});
