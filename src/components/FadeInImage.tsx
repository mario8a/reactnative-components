import React, { useState } from 'react';
import { ActivityIndicator, Animated, ImageStyle, StyleProp, View } from 'react-native';
import { useAnimation } from '../hooks/useAnimation';

interface Props {
  uri: string;
  style?: StyleProp<ImageStyle>;
}

export const FadeInImage = ({uri, style}: Props	) => {

  const {opacity, fadeIn} = useAnimation();
  const [isLoader, setisLoader] = useState(false);

  const finishLoading = () => {
    setisLoader(false);
    fadeIn();
  };

  return (
    <View style={{justifyContent: 'center', alignItems: 'center'}}>

    {
      isLoader && <ActivityIndicator style={{position: 'absolute'}} color="red" size={25} />
    }

      <Animated.Image
        source={{uri}}
        onLoadEnd={finishLoading}
        style={{
          ...style as any,
          opacity
        }}
      />
    </View>
  )
}
