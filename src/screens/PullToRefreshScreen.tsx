import React, { useState } from 'react'
import { RefreshControl, View } from 'react-native';
import { HeaderTitle } from '../components/HeaderTitle';
import { styles } from '../theme/appTheme';
import { ScrollView } from 'react-native-gesture-handler';

export const PullToRefreshScreen = () => {

  // const {top} = useSafeAreaInsets();

  const [refreshing, setRefreshing] = useState(false);
  const [data, setData] = useState<string>();

  const onRefresh = () => {
    setRefreshing(true);

    setTimeout(() => {
      console.log('Terminado');
      setRefreshing(false);
      setData('Holaaaa');
    }, 3500);
  };

  return (
    <ScrollView
    style={{
      // marginTop: refreshing ? top : 0
    }}
      refreshControl={
        <RefreshControl
          refreshing={refreshing}
          onRefresh={onRefresh}
          progressViewOffset={10} //margin top
          progressBackgroundColor="black"
          colors={['white', 'red', 'blue', 'orange']} //Android
          style={{backgroundColor: '#5856D6'}} // ios
          tintColor="white" //ios
          title="Regreshing.." //ios
          titleColor="white" //ios
        />
      }>
      <View style={styles.globalMargin}>
        <HeaderTitle title="PullToRefreshScreen" />
        {
          data && <HeaderTitle title={data} />
        }
      </View>
    </ScrollView>
  );
};

