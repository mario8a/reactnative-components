import React from 'react';
import { StyleSheet, TextInput, View, KeyboardAvoidingView, Platform, TouchableWithoutFeedback, Keyboard, Text } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { CustomSwitch } from '../components/CustomSwitch';
import { HeaderTitle } from '../components/HeaderTitle';
import { useForm } from '../hooks/useForm';
import { styles } from '../theme/appTheme';

export const TextInputScreen = () => {

  const {onChange, form, isSubscribe} = useForm({
    name: '',
    email: '',
    phone: '',
    isSubscribe: false,
  });

  // const [form, setForm] = useState({
  //   name: '',
  //   email: '',
  //   phone: '',
  // });

  // const onChange = (value: string, field: string) => {
  //   setForm({
  //     ...form,
  //     [field]: value,
  //   });
  // };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
    >
      <ScrollView>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.globalMargin}>
          <HeaderTitle title="Text input" />

          <TextInput
            style={stylesTextInput.inputStyle}
            placeholder="Ingrese su nombre"
            autoCorrect={false}
            autoCapitalize="words"
            onChangeText={(value) => onChange(value, 'name')}
          />
          <TextInput
            style={stylesTextInput.inputStyle}
            placeholder="Ingrese su email"
            autoCorrect={false}
            autoCapitalize="none"
            onChangeText={(value) => onChange(value, 'email')}
            keyboardType="email-address"
            keyboardAppearance="dark" //IOS
          />

          <View style={stylesTextInput.switchRow}>
            <Text style={{fontSize: 15}}> Suscribirse </Text>
            <CustomSwitch isOn={isSubscribe} onChange={(value) => onChange(value, 'isSubscribe')} />
          </View>

          <HeaderTitle title={JSON.stringify(form, null, 3)} />

          <HeaderTitle title={JSON.stringify(form, null, 3)} />

          <TextInput
            style={stylesTextInput.inputStyle}
            placeholder="Ingrese su telefono"
            onChangeText={(value) => onChange(value, 'phone')}
            keyboardType="phone-pad"

          />
          <View style={{height: 100}} />
        </View>

        </TouchableWithoutFeedback>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

const stylesTextInput = StyleSheet.create({
    inputStyle: {
      borderWidth: 1,
      borderColor: 'rgba(0,0,0,0.3 )',
      height: 50,
      paddingHorizontal: 20,
      borderRadius: 10,
      marginVertical: 10,
    },
    switchRow: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      alignItems: 'center',
      marginVertical: 10,
    },
});
