/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react'
import { Button, Modal, Text, View } from 'react-native';
import { HeaderTitle } from '../components/HeaderTitle';

export const ModalScreen = () => {

  const [isVisible, setIsVisible] = useState(false);


  return (
    <View>
      <HeaderTitle title="ModalScreen" />

      <Button
        title="Abrir Modal"
        onPress={() => setIsVisible(true)}
      />

      <Modal
        animationType="fade"
        visible={isVisible}
        transparent
      >

        <View style={{
          flex: 1,
          backgroundColor: 'rgba(0,0,0,0.3)',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
          <View style={{
            backgroundColor: 'white',
            width: 200,
            height: 200,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 10,
            shadowOffset: {
              width: 0,
              height: 10,
            },
            shadowOpacity: 0.25,
            elevation: 4,
          }}>
           <Text style={{
             fontSize: 20,
             fontWeight: 'bold',
           }}>Modal</Text>
            <Text> Cuerpo del modal </Text>
            <Button
              title="Cerrar"
              onPress={() => setIsVisible(false)}
            />
          </View>
        </View>

      </Modal>

    </View>
  )
}
