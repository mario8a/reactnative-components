/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react'
import { View, ActivityIndicator } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { FadeInImage } from '../components/FadeInImage';
import { HeaderTitle } from '../components/HeaderTitle';

export const InfiniteScrollScrenn = () => {

  const [numbers, setNumbers] = useState([0,1,2,3,4,5]);

  const loadMore = () => {
    const newArray: number[] = [];

    for ( let i = 0; i < 5; i++ ) {
      newArray[i] = numbers.length + i;
    }
    setTimeout(() => {
      setNumbers([...numbers, ...newArray]);
    }, 1500);
  };

  const renderItem = (item: number) => {
    return (
      <FadeInImage 
        uri={`https://picsum.photos/id/${item}/500/400`}
        style={{
          width: '100%',
          height: 400,
        }}  
      />
    );
  };

  return (
    <View style={{flex: 1}}>
      <FlatList
        data={numbers}
        keyExtractor={(item: any) => item.toString()}
        renderItem={({item}) => renderItem(item)}
        ListHeaderComponent={ <HeaderTitle title="infinite scroll" />}

        onEndReached={loadMore} // COn esto cargamos los elementos
        onEndReachedThreshold={0.5} //Indica que tan cerca del fin debe esar para traer la data

        ListFooterComponent={() => (
          <View style={{
            height: 150,
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
            <ActivityIndicator size={25} color="#5856D6" />
          </View>
        )}
      />
    </View>
  );
};

// const styles = StyleSheet.create({
//     textItem: {
//       height: 150,
//     }
// });